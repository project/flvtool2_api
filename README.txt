
The flvtool2_api module enables meta data writing for flv files. This
is helpful for getting file lengths, and other interaction with the 
flv file.

-----------------------------------------------------------------------
 INSTALLATION
-----------------------------------------------------------------------
This module relies on Ruby and FLVTool2 to write meta data
Download and install Ruby and  FLVTool2 on your server first:  
http://www.osflash.org/flvtool2

Next, install the module under sites/all/modules or 
sites/yoursite/modules.  How to install:  http://drupal.org/node/120641

Go to admin/build/modules

Enable the mm_flvtool2 module

The module will attempt to find Ruby and FLVTool2 automatically, however
you may need to configure the paths by hand.


-----------------------------------------------------------------------
 CONFIGURATION
-----------------------------------------------------------------------

Go to admin/settings/flvtool2_api

Make sure you configure the path to Ruby. This is relative to the root 
of your server. If don't know where Ruby is installed and you have
access to the command line of your server, you can run:

#which ruby

